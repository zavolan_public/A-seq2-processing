use strict;
use warnings;
use Getopt::Long;

if ( not defined $ARGV[0] ) {
  print STDERR "[ERROR] No input file.\n";
  exit(2);
}

my $minTPM              = 0;
my $TPM_clusters_gt_1nt = 0;
my $total_TPM           = 0;
my %data                = ();
my @LIBSIZE             = ();
my $N                   = 0;

my $upstream;
my $downstream;
my $nr_of_samples;

GetOptions(
  "upstream=i"   => \$upstream,
  "downstream=i" => \$downstream
);

if ( not defined $upstream or not defined $downstream ) {
  print STDERR "[ERROR] Usage: perl $0 --upstream=12 --downstream=12 3pSites.PAS.noBG.tsv.gz\n";
  exit(2);
}

# read counts for the samples are outlined in $ARGV[0]
# starting from the fourth column (== 3rd array pos).

open( F, "gunzip -c $ARGV[0] |" );

while (<F>) {
  chomp;
  if ( $_ =~ m/^#(.+)/ ) {
    my @F = split( /;/, $1 );
    $LIBSIZE[ $F[0] ] = $F[2];
    next;
  }
  my @F = split(/[\t\s]+/);
  $N++;

  if( not defined $nr_of_samples ) {
    my @tmp_ar =  @F[3 .. ($#F-2)];
    $nr_of_samples = scalar @tmp_ar;
  }

  my $OVERALL_TPM = 0;
  my $n_supported = 0;
  my @tmp         = ();

  #process the read counts per site from each sample
  foreach my $idx ( 3 .. $#F - 2 ) {
    my $tpm = $F[$idx] / $LIBSIZE[$idx] * 1000000;
    push @tmp, sprintf( "%.6f", $F[$idx] / $LIBSIZE[$idx] * 1000000 );
    $n_supported++ if ( $tpm > 0 );
    $OVERALL_TPM += $tpm;
  }

  my $key = "$F[0]:$F[1]";
  $data{$key} = [] if ( not defined $data{$key} );

  # save chr, strand, pos, overall tpm, number of supporting samples,
  # the poly(A) signals and the tpm values per sample
  push @{ $data{$key} }, [ $F[0], $F[1], $F[2], $OVERALL_TPM, $n_supported, $F[ $#F - 1 ], @tmp ];
}
close(F);

print STDERR "[INFO] $N sites read.\n";

my $n_clusters    = 0;
my $ip_candidates = 0;

# print header line
print "#chr\tstrand\tstart\tend\trep\ttotal_tpm\trepSite_signals";
foreach my $i (0 .. ($nr_of_samples - 1)) {
  print "\tsample_",$i,"_tpm";
}
print "\n";

foreach my $key ( sort keys %data ) {

  # sort sites by the number of samples that support it;
  # and then by TPM

  my @dataL = sort { $b->[4] <=> $a->[4] || $b->[3] <=> $a->[3] } @{ $data{$key} };

  # prepare a hash for easy access
  my %sitesHash = ();
  for ( my $i = 0 ; $i <= $#dataL ; $i++ ) {
    $sitesHash{ $dataL[$i]->[2] } = $i;
  }

  print STDERR "[INFO] Processing '$key'.\n[INFO] ";

  my @OUTPUT = ();
  for ( my $i = 0 ; $i <= $#dataL ; $i++ ) {
    next if ( not defined $dataL[$i] );

    my @cluster = ();
    my $start   = 0;
    my $end     = 0;
    if ( $dataL[$i]->[1] eq '+' ) {
      $start = $dataL[$i]->[2] - $upstream;
      $end   = $dataL[$i]->[2] + $downstream;
    } else {
      $start = $dataL[$i]->[2] - $downstream;
      $end   = $dataL[$i]->[2] + $upstream;
    }
    foreach my $j ( $start .. $end ) {
      if ( defined $sitesHash{$j} ) {
        my $k = $sitesHash{$j};
        next if ( not defined $dataL[$k] );
        my @deepcopy = ();
        foreach my $dc ( 0 .. $#{ $dataL[$k] } ) {
          push @deepcopy, $dataL[$k]->[$dc];
        }
        push @cluster, \@deepcopy;
        $dataL[$k] = undef;
      }
    }

    # get min and max cooridnates
    @cluster = sort { $a->[2] <=> $b->[2] } @cluster;
    my $strand = $cluster[0]->[1];
    my $min = $cluster[0]->[2];
    my $max = $cluster[$#cluster]->[2];
    my $len = $max - $min + 1;

    # get representative site
    # foreach of the samples determine the site with the highest read count
    my %rep         = ();
    my $N_samples   = 0;
    my $N_supported = 0;
    my @sumTPM      = ();

    # initialize sumTPM to zero for each sample; get total number of samples
    foreach my $idx ( 6 .. $#{ $cluster[0] } ) {
      push @sumTPM, 0;
      $N_samples++;
    }

    # iterate over all samples of the cluster
    my $sumTPMcnt = -1;
    foreach my $idx ( 6 .. $#{ $cluster[0] } ) {
      $sumTPMcnt++;

      # for each sample:
      # determine the site with the highest expression
      my @tmp = sort { $b->[$idx] <=> $a->[$idx] } @cluster;
      if ( $tmp[0]->[$idx] > 0 ) {
        $N_supported++;
        $rep{ $tmp[0]->[2] }++;
      }
      foreach my $curr_site (@tmp) {
        $sumTPM[$sumTPMcnt] += $curr_site->[$idx];
      }
    }

    foreach my $idx ( 0 .. $#sumTPM ) {
      $sumTPM[$idx] = sprintf( "%.6f", $sumTPM[$idx] );
    }

    # define the representative site as follows:
    # the site that was most often top expressed across all samples
    # in case of equality: take the most downstream
    my @R;
    if ( $strand eq "+" ) {
      @R = sort { $rep{$b} <=> $rep{$a} || $b <=> $a } keys %rep;
    } else {
      @R = sort { $rep{$b} <=> $rep{$a} || $a <=> $b } keys %rep;
    }
    my $rep = $R[0];
    ## don't report the fraction of samples that support the rep site
    ## and the cluster
    # my $rep_support = sprintf( "%.2f", $rep{ $R[0] } / $N_samples );
    # $N_supported = sprintf( "%.2f", $N_supported / $N_samples );
    my $rep_PAS = 'NA';
    foreach my $idx ( 0 .. $#cluster ) {
      if ( $cluster[$idx]->[2] eq $rep ) {
        $rep_PAS = $cluster[$idx]->[5];
        last;
      }
    }

    # poly(A) signal (PAS) string example:
    # AATAAA@-29@125193;AATATA@-24@125198

    # look for IP within the cluster
    # here, IP is defined as:
    # any of the cleavage sites is located directly
    # on one of the nucleotides of the PAS
    # or the PAS is immediately downstream of CS
    my $ipCandidate = 0;
  LABEL: foreach my $cl ( 0 .. $#cluster ) {
      my $pasString = $cluster[$cl]->[5];
      next if ( $pasString eq 'NA' );
      my @signals = split( ";", $pasString );
      foreach my $sig (@signals) {
        ( undef, my $pos ) = split( "@", $sig );

        if ( $pos >= -6 ) {
          $ipCandidate = 1;
          last LABEL;
        }
      }
    }

    if ($ipCandidate) {
      $ip_candidates++;

      # define the most downstream site as rep site
      # and add "IPCandidate" to the PAS string
      if ( $strand eq "+" ) {
        @cluster = sort { $b->[2] <=> $a->[2] } @cluster;

        #update rep_site
        $rep         = $cluster[0]->[2];
        # $rep_support = 0;
        # $N_supported = 0;
        $rep_PAS     = $cluster[0]->[5];
        $rep_PAS .= ";IPCandidate";
      } else {
        @cluster = sort { $a->[2] <=> $b->[2] } @cluster;

        #update rep_site
        $rep         = $cluster[0]->[2];
        # $rep_support = 0;
        # $N_supported = 0;
        $rep_PAS     = $cluster[0]->[5];
        $rep_PAS .= ";IPCandidate";
      }
    }

    # get the overall TPM for this cluster
    my $cluster_tpm = 0;
    foreach my $entry (@cluster) {
      $cluster_tpm += $entry->[3];
    }

    # prepare output
    if ( $cluster_tpm >= $minTPM ) {
      $n_clusters++;
      $TPM_clusters_gt_1nt += $cluster_tpm if ( $len > 1 );
      $total_TPM += $cluster_tpm;
      push @OUTPUT, [
        $cluster[0]->[0],
        $cluster[0]->[1],
        $min, $max, $rep, $cluster_tpm, $rep_PAS, @sumTPM
        ];
    }
  }

  @OUTPUT = sort { $a->[4] <=> $b->[4] } @OUTPUT;
  foreach my $entry (@OUTPUT) {
    print join( "\t", @{$entry} ), "\n";
  }
  print STDERR "\n";
}
my $pc = sprintf( "%.2f", $TPM_clusters_gt_1nt / $total_TPM * 100 );
print STDERR
  "[INFO] $ARGV[0]: $n_clusters clusters (percent of TPM associated with cluster larger than 1nt: $pc)\n",
  "[INFO] inferred with d=$upstream/$downstream.\n";
print STDERR "[INFO] $ip_candidates clusters marked as internal priming candidates\n";

