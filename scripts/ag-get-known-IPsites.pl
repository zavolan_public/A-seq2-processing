use strict;
use warnings;
use Getopt::Long;

my @files = ();
my $help;
GetOptions(
  "help"   => \$help,
  "h"      => \$help
);

# process options
@files = @ARGV;
my $showhelp = 0;
$showhelp = 1 if ( $#files == -1 );
$showhelp = 1 if ($help);

if ($showhelp) {
  print STDERR "Usage: $0 --file=file1 --file=file2\n";
  exit 1;
}

my %sites = ();
foreach my $idx ( 0 .. $#files ) {
  my $sum = 0;
  open( F, "gunzip -c $files[$idx] |" );
  while (<F>) {
    chomp;
    my @F = split(/\t/);
    if ( $F[3] eq 'IP' ) {
      $sites{"$F[0]:$F[5]"} = {} if ( not defined $sites{"$F[0]:$F[5]"} );
      $sites{"$F[0]:$F[5]"}->{ $F[2] } = 1;
    }
  }
  close(F);
}

my $cnt = 0;
foreach my $key ( sort keys %sites ) {
  my @X = split( /:/, $key );
  foreach my $entry ( sort { $a <=> $b } keys %{ $sites{$key} } ) {
    $cnt++;
    my $s = $entry - 1;
    print "$X[0]\t$s\t$entry\t$cnt\t0\t$X[1]\n";
  }
}
