use strict;
use warnings;
use Getopt::Long;

my $help;
my $fastq_to_fasta;
my $fastx_renamer;
my $outfile_name;

GetOptions(
  "fastq_to_fasta=s" => \$fastq_to_fasta,
  "fastx_renamer=s"  => \$fastx_renamer,
  "outfile=s"        => \$outfile_name,
  "help"             => \$help,
  "h"                => \$help
);

my $showhelp = 0;
$showhelp = 1 if ( not defined $ARGV[0] );
$showhelp = 1 if ( defined $help );
$showhelp = 1 if ( not defined $fastq_to_fasta );
$showhelp = 1 if ( not defined $fastx_renamer );
$showhelp = 1 if ( not defined $outfile_name );

if ($showhelp) {
  print STDERR "Usage: perl $0 --fastq_to_fasta=path --fastx_renamer=path ";
  print STDERR "--outfile=name_of_outfile.fa.gz file.fa.gz\n\n";
  exit;
}

if ( not -e $ARGV[0] ) {
  print STDERR "[ERROR] File '$ARGV[0]' not found.\n";
  exit;
}

if ( $ARGV[0] !~ m/(f|fast)q\.gz$/ ) {
  print STDERR "[ERROR] File '$ARGV[0]' does not look like a fq.gz file.\n";
  exit;
}

my $exe_found = `which $fastq_to_fasta 2>&1`;
if ( $exe_found =~ /no $fastq_to_fasta in/ ) {
  print STDERR "[ERROR] Please check path to fastq_to_fasta executable.\n";
  print STDERR "[ERROR] $fastq_to_fasta\n";
  exit(2);
}

$exe_found = `which $fastx_renamer 2>&1`;
if ( $exe_found =~ /no $fastx_renamer in/ ) {
  print STDERR "[ERROR] Please check path to fastx_renamer executable.\n";
  exit(2);
}

`zcat $ARGV[0] | $fastq_to_fasta -Q33 | $fastx_renamer -n COUNT -z > $outfile_name`;

