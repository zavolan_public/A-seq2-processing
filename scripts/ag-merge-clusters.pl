use strict;
use warnings;
use Getopt::Long;

my $minDistToPAS;
my $maxClusterSize;

GetOptions(
  "minDistToPAS=i" => \$minDistToPAS,
  "maxsize=i"      => \$maxClusterSize
);

if( not defined $minDistToPAS or not defined $maxClusterSize ) {
  print STDERR "[ERROR] Usage: perl $0 --minDistToPAS=15 --maxsize=25 clusters.primary.tsv.gz\n\n";
  exit(2);
}

if( $ARGV[0] !~ /gz$/) {
  print STDERR "[ERROR] input file does not seem to be in gzipped format ($ARGV[0] should end with .gz)\n\n";
  exit(2);
}

### ATTENTION ###
# This script expects the input file to be formatted
# according to ag-generate-clusters.pl from the A-seq-processing pipeline
# -> all data is accessed with hard coded indices

my $header;
my %clusters = ();
open( F, "gzip -dc $ARGV[0] |" ) or die "Can't open pipe to $ARGV[0]\n";
while (<F>) {
  chomp;
  if ( $_ =~ /^#/ ) {
    $header = $_;
    next;
  }
  my @F = split(/\t/);
  $clusters{"$F[0]:$F[1]"} = [] if ( not defined $clusters{"$F[0]:$F[1]"} );
  push @{ $clusters{"$F[0]:$F[1]"} }, \@F;
}
close(F);

# print header line
print "$header\n";

my $cnt_merged      = 0;
my $cnt_merged2     = 0;
my $cnt_merged3     = 0;
my $cnt_merged4     = 0;
my $cnt_merged5     = 0;
my $cnt_NA_deleted  = 0;
my $cnt_IP_deleted  = 0;
my $cnt_IP_resolved = 0;
my $clusterSelfHelp = 0;

# save the length of clusters after merging because of same poly(A) signals
my @lengthPASmergedCL = ();

foreach my $region ( sort keys %clusters ) {
  my @data = @{ $clusters{$region} };

  # note: entries in @data are ordered in increasing
  # genomic position due to the format of the input file clusters.tsv

  my %order = ();
  for ( my $i = 0 ; $i <= $#data ; $i++ ) {
    $order{$i} = $data[$i]->[5];
  }

  # initially, process internal priming candidates
  ( undef, my $strand ) = split( ":", $region );
  if( $strand eq "-" ) {
    # reverse data array to iterate over it 5' -> 3'
    @data = reverse(@data);
  }

  for ( my $i = 0 ; $i <= $#data ; $i++ ) {
    my $repSite = $data[$i]->[4];
    # get the poly(A) signal (PAS) string
    my @rep = split( /;/, $data[$i]->[6] );

    #skip site if it is not marked as internal priming
    next if ( $rep[$#rep] !~ /^IP/ );
    my %PAS = ();

    # save all PAS-positions in the current IP-candidate-cluster
    foreach my $entry (@rep) {
      if ( $entry =~ m/(.*@)(\d+)$/ ) {
	$PAS{$2}++;
      }
    }

    # find next downstream cluster that has no internal priming
    my $found = 1;
    my $k     = $i;
    while ($found) {
      $k++;
      if ( $k <= $#data ) {
	if ( $data[$k]->[6] !~ /IP/ ) {
	  $found = 0;
	}
      } else {
	$k     = undef;
	$found = 0;
      }
    }
    if ( defined $k ) {

      # consistency check
      if( not defined $data[$k]->[6] ) {
	print STDERR "[ERROR] No valid downstream cluster inferred\n";
	print STDERR "[ERROR] assumed downstream cluster:\n", join(",", @{$data[$k] }), "\n[ERROR] k: $k\n";
	exit(2);
      }

      # $k is set to the downstream cluster the current cluster is compared to
      # check downstream cluster
      my $noMergePossible = 0;
      my $samePAS         = 0;
      my @repDS           = split( /;/, $data[$k]->[6] );
      foreach my $entry (@repDS) {
	if ( $entry eq 'NA' ) {
	  $noMergePossible = 1;
	  last;
	}
	if ( $entry =~ m/(.*@)(\d+)$/ ) {

	  # check whether this location for a PAS is also used within the current cluster
	  if ( defined $PAS{$2} ) {
	    $samePAS = 1;
	  }

	  if ( ( ($strand eq "+" and $2 > $repSite ) or
		 ($strand eq "-" and $2 < $repSite ) ) and not defined $PAS{$2} ) {

	    # downstream cluster has own PAS, independet of upstream putative IP candidate
	    $noMergePossible = 1;
	    last;
	  }
	}
      }

      # check whether merging is possible
      if ( not $noMergePossible and $samePAS ) {

	# downstream cluster shares its PAS with the cluster under observation
	my $new_cl = _merge_IP( $data[$i], $data[$k] );
	$cnt_IP_resolved++;
	$data[$k] = $new_cl;
	$data[$i] = undef;
      }
    }

    # next case: cluster still exists and no merge was done
    if ( defined $data[$i] ) {

      # check whether the distance of the
      # representative site to the closest PAS
      # is bigger than x nt (standard x: $minDistToPAS = 15)
      my @positions = sort { $b <=> $a } keys %PAS;
      if ( abs( $repSite - $positions[0] ) >= $minDistToPAS ) {

	# cluster remains in the atlas, IPCandidate note is removed
	my @tmp = @rep[ 0 .. $#rep - 1 ];
	$data[$i]->[6] = join( ";", @tmp );

	$clusterSelfHelp++;

      } else {

	# cluster is discarded
	$data[$i] = undef;
	$cnt_IP_deleted++;
      }
    }
  }

  # if strand is "-", change back @data entries to increasing genomic positions
  if( $strand eq "-" ) {
    @data = reverse(@data);
  }

  #-----------------------------------------------------------------------------
  # Finished IP-processing; next: same PAS merging
  #-----------------------------------------------------------------------------

  # (downstream cluster needs to have the same signals
  # as the cluster upstream of it )

  for ( my $i = 0 ; $i <= $#data - 1 ; $i++ ) {
    next if ( not defined $data[$i] );
    my $merge_flag = 0;
    my $k;
    my $tmp    = $i + 1;
    my $search = 1;
    while ( $search and $tmp <= $#data ) {
      if ( defined $data[$tmp] ) {
        $k      = $tmp;
        $search = 0;
      }
      $tmp++;
    }
    if ( defined $k ) {
      if ( not defined $data[$k]->[4] ) {
        print STDERR "[ERROR] data for $k inconsistent; no representative site found\n[ERROR] ", join(",", @{ $data[$k] }), "\n";
        exit(2);
      }
      if ( $data[$i]->[6] ne 'NA' and $data[$k]->[6] ne 'NA' ) {

        # check if the representative sites are driven by the same signals
        # @repA is marked as upstream, @repB as downstream cluster
        my @repA = ();
        my @repB = ();
        if ( $strand eq "+" ) {
          @repA = split( /;/, $data[$i]->[6] );
          @repB = split( /;/, $data[$k]->[6] );
        } elsif ( $strand eq "-" ) {
          @repA = split( /;/, $data[$k]->[6] );
          @repB = split( /;/, $data[$i]->[6] );
        } else {
          print STDERR "[ERROR] Could not infer strand information from $region\n";
          exit(2);
        }

        # save the PAS for the upstream cluster and merge both if the downstream
        # has only annotated PAS already found for the upstream cluster
        my %PAS         = ();
        my $PAS_present = 0;
        my $flag        = 1;
        foreach my $entry (@repA) {
          if ( $entry =~ m/(.*@)(\d+)$/ ) {
            $PAS{$2}++;
          }
        }
	# iterate over the downstream cluster PAS
        foreach my $entry (@repB) {
          $PAS_present = 1;
          if ( $entry =~ m/(.*@)(\d+)$/ ) {
            if ( not defined $PAS{$2} ) {
	      # donwstream cluster has independent PAS
              $flag = 0;
              last;
            }
          }
        }

        $merge_flag = 1 if ( $flag == 1 and $PAS_present == 1 );
      }

      if ( $merge_flag == 1 ) {
        $cnt_merged++;
        my $new_cl = _merge( $data[$i], $data[$k] );
        $data[$k] = $new_cl;
        $data[$i] = undef;

        # adjust array with cluster sizes for merged clusters
        $lengthPASmergedCL[$i] = 0;
        $lengthPASmergedCL[$k] = ($data[$k]->[3] - $data[$k]->[2]) + 1;
      }
    }
  }

  #-----------------------------------------------------------------------------
  # Finished same PAS merging; next: merge if two clusters are below maxsize
  #-----------------------------------------------------------------------------

  # next check if the max cluster length is not yet reached
  for ( my $i = 0 ; $i <= $#data - 1 ; $i++ ) {
    next if ( not defined $data[$i] );
    my $nexti;
    my $tmp    = $i + 1;
    my $search = 1;
    while ( $search and $tmp <= $#data ) {
      if ( defined $data[$tmp] ) {
        $nexti  = $tmp;
        $search = 0;
      }
      $tmp++;
    }
    if ( defined $nexti ) {

      # check if the resulting cluster is not growing too big
      my $newL       = $data[$nexti]->[3] - $data[$i]->[2] + 1;
      my $merge_flag = 0;

      # maximum length of cluster
      if ( $newL <= $maxClusterSize ) {
        $merge_flag = 1;

	# compare to PASs of both clusters
	# prevent merging, if the downstream cluster has its own PAS
	my @repUp = ();
	my @repDown = ();
	my %PAStmp = ();
	if($strand eq "+") {
	  @repUp = split( /;/, $data[$i]->[6] );
	  @repDown = split( /;/, $data[$nexti]->[6] );
	} elsif( $strand eq "-") {
	  @repUp = split( /;/, $data[$nexti]->[6] );
	  @repDown = split( /;/, $data[$i]->[6] );
	} else {
	  print STDERR "[ERROR] Could not infer strand information from $region\n";
	  exit;
	}
	foreach my $entry ( @repUp ) {
	  if ( $entry =~ m/(.*@)(\d+)$/ ) {
	    $PAStmp{$2}++;
	  }
	}
	foreach my $entry ( @repDown ) {
	  if ( $entry =~ m/(.*@)(\d+)$/ ) {
	    if (not defined $PAStmp{ $2 } and $entry ne "NA") {
	      # the downstream cluster has a PAS
	      # not defined for the upstream cluster
	      $merge_flag = 0;
	    }
	  }
	}
      }

      if ( $merge_flag == 1 ) {
        $cnt_merged2++;
        my $new_cl = _merge( $data[$i], $data[$nexti] );
        $data[$nexti] = $new_cl;
        $data[$i]     = undef;
      }
    }
  }

  # current state: do not further eliminate closely spaced clusters without
  # any poly(A) signal

  # # process NA clusters that are close together
  # my @naCluster      = ();
  # my @singleClusters = ();
  # for ( my $i = 0 ; $i <= $#data - 1 ; $i++ ) {
  #   next if ( not defined $data[$i] );

  #   if ( $data[$i]->[6] ne "NA" ) {

  #     # treat previous NA cluster(s)
  #     if ( $#naCluster > -1 ) {
  #       if ( $naCluster[3] - $naCluster[2] <= ( $maxClusterSize * 2 ) ) {

  #         # merge all clusters
  #         if ( $#singleClusters > 0 ) {
  #           foreach my $entry ( 0 .. $#singleClusters - 1 ) {
  #             my $newone =
  #               _merge( $data[ $singleClusters[$entry] ], $data[ $singleClusters[ $entry + 1 ] ] );
  #             $data[ $singleClusters[$entry] ] = undef;
  #             $data[ $singleClusters[ $entry + 1 ] ] = $newone;
  #           }
  #           $cnt_merged3++;

  #         } else {

  #           # no merge necessary since the last NA cluster was alone
  #           # nothing needs to be done
  #         }

  #       } else {

  #         # discard all clusters
  #         foreach my $entry ( 0 .. $#singleClusters ) {
  #           $data[ $singleClusters[$entry] ] = undef;
  #           $cnt_NA_deleted++;
  #         }
  #       }
  #       @naCluster      = ();
  #       @singleClusters = ();
  #     }
  #     next;
  #   }

  #   # treat NA clusters

  #   # simply save the cluster if there is no other cluster to compare against
  #   if ( $#naCluster == -1 ) {
  #     @naCluster = @{ $data[$i] };
  #     push @singleClusters, $i;
  #     next;
  #   }

  #   # if this part is reached at least the second NA cluster in a row is processed
  #   if ( $data[$i]->[2] - $naCluster[3] <= 12 ) {

  #     # NA clusters are close together; create preliminary association
  #     $naCluster[3] = $data[$i]->[3];
  #     push @singleClusters, $i;

  #   } else {

  #     # distance too big
  #     # process previous NA cluster(s)
  #     if ( $naCluster[3] - $naCluster[2] <= ( $maxClusterSize * 2 ) ) {

  #       # merge all clusters
  #       if ( $#singleClusters > 0 ) {
  #         foreach my $entry ( 0 .. $#singleClusters - 1 ) {
  #           my $newone =
  #             _merge( $data[ $singleClusters[$entry] ], $data[ $singleClusters[ $entry + 1 ] ] );
  #           $data[ $singleClusters[$entry] ] = undef;
  #           $data[ $singleClusters[ $entry + 1 ] ] = $newone;
  #         }
  #         $cnt_merged3++;
  #       } else {

  #         # no merge necessary since the last NA cluster was alone
  #         # nothing needs to be done
  #       }

  #     } else {

  #       # discard all clusters
  #       foreach my $entry ( 0 .. $#singleClusters ) {
  #         $data[ $singleClusters[$entry] ] = undef;
  #         $cnt_NA_deleted++;
  #       }
  #     }
  #     @naCluster      = @{ $data[$i] };
  #     @singleClusters = ($i);
  #   }
  # }

  # # treat last NA cluster, if available
  # if ( $#naCluster > -1 ) {
  #   if ( $naCluster[3] - $naCluster[2] <= ( $maxClusterSize * 2 ) ) {

  #     # merge all clusters
  #     if ( $#singleClusters > 0 ) {
  #       foreach my $entry ( 0 .. $#singleClusters - 1 ) {
  #         my $newone =
  #           _merge( $data[ $singleClusters[$entry] ], $data[ $singleClusters[ $entry + 1 ] ] );
  #         $data[ $singleClusters[$entry] ] = undef;
  #         $data[ $singleClusters[ $entry + 1 ] ] = $newone;
  #       }
  #       $cnt_merged3++;
  #     } else {

  #       # no merge necessary since the last NA cluster was alone
  #       # nothing needs to be done
  #     }

  #   } else {

  #     # discard all clusters
  #     foreach my $entry ( 0 .. $#singleClusters - 1 ) {
  #       $data[ $singleClusters[$entry] ] = undef;
  #       $cnt_NA_deleted++;
  #     }
  #   }
  #   @naCluster      = ();
  #   @singleClusters = ();
  # }

  foreach my $entry (@data) {
    if ( defined $entry ) {
      print join( "\t", @{$entry} ), "\n";
    }
  }
}

print STDERR
  "[INFO] $cnt_merged (same PAS) + $cnt_merged2 (below maximum length of $maxClusterSize) + $cnt_merged3 (noPAS CLs closer than 12nt to each other) entries merged.\n";
print STDERR "[INFO] $cnt_NA_deleted clusters without PAS were discarded\n";
print STDERR
  "[INFO] $cnt_IP_resolved putative internal priming clusters resolved (merged or internally solved)\n";
print STDERR "[INFO] $cnt_IP_deleted putative internal priming clusters deleted\n";
print STDERR
  "[INFO] $clusterSelfHelp IP candidates rescued since most downstream site with more than $minDistToPAS nt distance to next PAS\n";

my $CLlength_PASmergedCL = 0;
my $number               = 0;
foreach my $i ( 0 .. $#lengthPASmergedCL ) {
  if ( defined $lengthPASmergedCL[$i] and $lengthPASmergedCL[$i] > 0 ) {
    $CLlength_PASmergedCL += $lengthPASmergedCL[$i];
    $number++;
  }
}
print STDERR "[INFO] Number of clusters that emerged from same-PAS-clustering: $number\n";
print STDERR "[INFO] Average cluster length of these clusters: ",
  sprintf( "%.4f", $CLlength_PASmergedCL / $number ), "\n";

sub _merge {
  my $A = $_[0];
  my $B = $_[1];

  # determine the leader ($A entry has higher total TPM value)
  if ( $A->[5] < $B->[5] ) {
    $A = $_[1];
    $B = $_[0];
  }

  # get chr and strand
  my @C = ( $A->[0], $A->[1] );

  # define min start and max end
  my $minS = ( $A->[2] < $B->[2] ) ? $A->[2] : $B->[2];
  my $maxE = ( $A->[3] > $B->[3] ) ? $A->[3] : $B->[3];
  push @C, $minS;
  push @C, $maxE;

  # take representative site from leader cluster
  push @C, $A->[4];

  # sum up both tmp values
  push @C, $A->[5] + $B->[5];

  # take the PAS string from $A
  push @C, $A->[6];

  # for each sample: sum up the expression of $A and $B
  foreach my $i ( 7 .. $#{$A} ) {
    push @C, $A->[$i] + $B->[$i];
  }

  return \@C;
}

sub _merge_IP {

  # $A is set as leader
  my $A = $_[1];
  my $B = $_[0];

  # get chr and strand
  my @C = ( $A->[0], $A->[1] );

  # define min start and max end
  my $minS = ( $A->[2] < $B->[2] ) ? $A->[2] : $B->[2];
  my $maxE = ( $A->[3] > $B->[3] ) ? $A->[3] : $B->[3];
  push @C, $minS;
  push @C, $maxE;

  # take representative site from leader cluster
  push @C, $A->[4];

  # sum up both tmp values
  push @C, $A->[5] + $B->[5];

  # take the PAS string from $A
  push @C, $A->[6];

  # for each sample: sum up the expression of $A and $B
  foreach my $i ( 7 .. $#{$A} ) {
    push @C, $A->[$i] + $B->[$i];
  }

  return \@C;
}
