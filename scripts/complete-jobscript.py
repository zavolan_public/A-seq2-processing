from distutils.spawn import find_executable
from argparse import ArgumentParser, RawTextHelpFormatter

parser = ArgumentParser(description=__doc__, formatter_class=RawTextHelpFormatter)
parser.add_argument("-i",
                    "--input",
                    dest="input",
                    default="jobscript.sh",
                    help="name of jobscript [default: jobscript.sh]")
                    
parser.add_argument("-o",
                    "--output",
                    dest="output",
                    default="jobscript.withJOBID.withPATH.sh",
                    help=("name of edited jobscirpt " + 
                           "[default: jobscript.withJOBID.withPATH.sh]") )

options = parser.parse_args()

path_prefix = find_executable("snakemake")
path_prefix = "/".join(path_prefix.split("/")[:-1])
with open(options.output, "w") as out:
    with open(options.input, "r") as ifile:
        for line in ifile:
            out.write("%s" % line)
with open(options.output, "a") as out:
    out.write("PATH=" + path_prefix + r':${{PATH}}' + "\n")
    out.write("export PATH\n\n")
    out.write(r'echo -e "JOB ID\t$JOB_ID"' + "\n")
    out.write(r'echo "=============================="' + "\n")
    out.write(r'echo -e "rule\t$JOB_NAME"' + "\n")
    out.write(r'echo -e "==============================\n"' + "\n\n")
    out.write(r'{exec_job}' + "\n")
