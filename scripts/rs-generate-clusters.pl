use strict;
use warnings;
use Getopt::Long;

my $TPM_clusters_gt_1nt = 0;
my $total_TPM           = 0;
my $upstream;
my $downstream;
my %data    = ();
my @LIBSIZE = ();
my $n       = 0;
my $N       = 0;

GetOptions(
  "upstream=i"   => \$upstream,
  "downstream=i" => \$downstream
);

if ( not defined $ARGV[0] or not defined $upstream or not defined $downstream ) {
  print STDERR "[ERROR] Usage: perl $0 --upstream=25 --downstream=25 3pSites.PAS.noBG.tsv.gz\n";
  exit;
}

# read counts for the samples are outlined in $ARGV[0] from fourth column on (== 3rd array pos). Hence, idx is initialized with two and incremented before the first time used
my $idx = 2;

open( F, "gunzip -c $ARGV[0] |" );

while (<F>) {
  chomp;
  if ( $_ =~ m/^#(.+)/ ) {
    my @F = split( /;/, $1 );
    $LIBSIZE[ $F[0] ] = $F[2];
    next;
  }
  my @F = split(/\t/);
  $N++;

  my $OVERALL_TPM  = 0;
  my $n_supported  = 0;
  my @tmpTissue    = ();
  my @tpmPerSample = ();

  #process the read counts per site from each sample
  foreach my $sample ( 3 .. $#F - 2 ) {
    my $tpm = $F[$sample] / $LIBSIZE[$sample] * 1000000;

    push @tpmPerSample, $tpm;
    if ( $F[$sample] > 0 ) {

      $OVERALL_TPM += $tpm;
      $n_supported++;
    }
  }

  $n++;
  my $key = "$F[0]:$F[1]";
  $data{$key} = [] if ( not defined $data{$key} );

  # save chr, strand, pos, overall tpm, number of supporting samples and
  # the tpm per sample
  push @{ $data{$key} }, [ $F[0], $F[1], $F[2], $OVERALL_TPM, $n_supported, \@tpmPerSample ];
}
close(F);

print STDERR "[INFO] $N sites read.\n";
print STDERR "[INFO] $n sites used.\n";

my $n_clusters    = 0;
my $ip_candidates = 0;
foreach my $key ( sort keys %data ) {

  # sort sites by the number of samples that support it;
  # and then by TPM

  my @dataL = sort { $b->[4] <=> $a->[4] || $b->[3] <=> $a->[3] } @{ $data{$key} };

  # prepare a hash for easy access
  my %sitesHash = ();
  for ( my $i = 0 ; $i <= $#dataL ; $i++ ) {

    # save the pos in the array of each genomic pos
    $sitesHash{ $dataL[$i]->[2] } = $i;
  }

  print STDERR "[INFO] Processing '$key'.\n[INFO] ";

  my @OUTPUT = ();
  for ( my $i = 0 ; $i <= $#dataL ; $i++ ) {
    next if ( not defined $dataL[$i] );

    my @cluster = ();
    my $start   = 0;
    my $end     = 0;
    if ( $dataL[$i]->[1] eq '+' ) {
      $start = $dataL[$i]->[2] - $upstream;
      $end   = $dataL[$i]->[2] + $downstream;
    } else {
      $start = $dataL[$i]->[2] - $downstream;
      $end   = $dataL[$i]->[2] + $upstream;
    }
    foreach my $j ( $start .. $end ) {
      if ( defined $sitesHash{$j} ) {
        my $k = $sitesHash{$j};

        # dataL[$k] contains the entry for the genomic pos $j
        next if ( not defined $dataL[$k] );
        my @deepcopy = ();
        foreach my $dc ( 0 .. $#{ $dataL[$k] } ) {
          push @deepcopy, $dataL[$k]->[$dc];
        }

        # @cluster stores all sites in the region defined as belonging to that cluster
        push @cluster, \@deepcopy;
        $dataL[$k] = undef;
      }
    }

    # get min and max cooridnates
    @cluster = sort { $a->[2] <=> $b->[2] } @cluster;
    my $min = $cluster[0]->[2];
    my $max = $cluster[$#cluster]->[2];
    my $l   = $max - $min + 1;

    # get representative site
    # use the highest expressed site
    my %rep;
    my $N_samples   = 0;
    my $N_supported = 0;
    my @sumTPM      = ();

    #initialze @sumTPM to zero for each sample; get total number of samples
    foreach my $idx ( 5 .. $#{ $cluster[0] } ) {
      push @sumTPM, 0;
      $N_samples++;
    }
    my $sumTPMcnt = -1;
    foreach my $idx ( 5 .. $#{ $cluster[0] } ) {
      $sumTPMcnt++;

      # iterate over all samples:
      # save for each sample the rep site
      my @tmp = sort { $b->[$idx] <=> $a->[$idx] } @cluster;
      if ( $tmp[0]->[$idx] > 0 ) {
        $N_supported++;
        $rep{ $tmp[0]->[2] }++;
      }
      foreach my $sample (@cluster) {
        $sumTPM[$sumTPMcnt] += $sample->[$idx];
      }
    }

    my @R = sort { $rep{$b} <=> $rep{$a} } keys %rep;
    my $rep = $R[0];

    ###
    # prepare output array
    ###

    # get TPM
    my $tpm = 0;
    foreach my $entry (@cluster) {
      $tpm += $entry->[3];
    }

    $n_clusters++;
    $TPM_clusters_gt_1nt += $tpm if ( $l > 1 );
    $total_TPM += $tpm;
    push @OUTPUT,
      [ $cluster[0]->[0], $cluster[0]->[1], $min, $max, $rep, $l, $tpm, @sumTPM ];

  }
  @OUTPUT = sort { $a->[4] <=> $b->[4] } @OUTPUT;
  foreach my $entry (@OUTPUT) {
    print join( "\t", @{$entry} ), "\n";
  }
  print STDERR "\n";
}

# print percent of clusters greater than 1 nt
my $pc = sprintf( "%.2f", $TPM_clusters_gt_1nt / $total_TPM * 100 );
print STDERR
  "[INFO] $ARGV[0]: $n_clusters clusters ($TPM_clusters_gt_1nt/$total_TPM=$pc) inferred with d=$upstream/$downstream.\n";
