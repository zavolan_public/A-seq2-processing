use strict;
use warnings;

my %ids        = ();
my $header     = '';
my $header_cnt = 0;
my $cnt        = 0;

my %duplicate_count = ();

# count UMI duplicates
my $UMI_duplicates = 0;
# count total reads
my $total = 0;

my $UMI;
my $name;
while(<STDIN>) {
  chomp;
  $cnt++;
  if( $_ =~ /^>(.*)/) {
    ($name, $UMI) = split("-", $1);
    $header_cnt = $cnt;
    next;
  }
  if( $header_cnt + 1 != $cnt ) {
    print STDERR "[ERROR] Script only works with FASTA 1-liners.\n";
    exit(2);
  }
  $total++;
  my $umi_read = "$UMI:$_";
  if( defined $ids{$umi_read} ) {
    $UMI_duplicates++;
    $duplicate_count{ $umi_read} +=1;
    next;
  }
  $ids{$umi_read} = $name;
  $duplicate_count{$umi_read} = 1;
}

print STDERR "[INFO] Finished collapsing reads with same UMI\n";
print STDERR "[INFO] Total number of reads: $total\n";
print STDERR "[INFO] Number of kept sequences: ", scalar keys %ids, "\n";
my @tmp = sort{ $duplicate_count{$b} <=> $duplicate_count{$a} } keys %duplicate_count;
print STDERR "[INFO] Maximum number of times a sequence was collapsed:\n[INFO] (occured for $tmp[0])\n[INFO] ", $duplicate_count{ $tmp[0] }, "\n";

foreach my $k (sort{ $ids{$a} <=> $ids{$b} } keys %ids ) {
  my $n = $ids{$k};
  (my $umi, my $read) = split(":", $k);
  print ">$n:$umi\n$read\n";
}

print STDERR "[INFO] Number of discarded reads due to same UMI-read-combination: $UMI_duplicates\n";
