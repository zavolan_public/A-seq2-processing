use strict;
use warnings;
use Getopt::Long;

my $count_out;

GetOptions( "count_out=s" => \$count_out );

if ( not defined $ARGV[0] or not defined $count_out ) {
  print STDERR "[ERROR] Usage: perl $0 --count_out=count.stats.out reads.mapped.sorted.bed.gz\n\n";
  exit(2);
}

my $collapse_events = 0;
my %cnt = ( "unique" => 0, "multi" => 0 );
my %cur_pos = ();
my %multimappers = ();

open( F, "gzip -dc $ARGV[0] |" ) or die "Can't open pipe to $ARGV[0]\n";
while (<F>) {
  chomp;
  my @F   = split("\t");
  my $pos = "$F[0]:$F[5]:$F[1]:$F[2]";
  if ( not defined $cur_pos{$pos} ) {

    # treat previous pos
    if ( scalar keys %cur_pos == 1 ) {
      foreach my $entry ( keys %cur_pos ) {
        foreach my $umi ( keys %{ $cur_pos{$entry} } ) {

	  if( $cur_pos{$entry}->{$umi}->[4] == 1.0 ) {
	    $cnt{ "unique" } += 1;
	  } elsif ( $cur_pos{$entry}->{$umi}->[4] < 1.0 ) {
	    $multimappers{ $cur_pos{$entry}->{$umi}->[3] } = 1;
	  } else{
	    print STDERR "[ERROR] Current read is neither unique nor multi mapper based on count value\n";
	    print STDERR "[ERROR] ", join("\t", @{ $cur_pos{$entry}->{$umi} } ), "\n";
	    exit(2);
	  }
          print join( "\t", @{ $cur_pos{$entry}->{$umi} } ), "\n";
        }
        %cur_pos = ();
      }
    } elsif ( scalar keys %cur_pos > 1 ) {
      print STDERR "[ERROR] More than one previous position was not handled\n";
      exit(2);
    }
  }

  # get UMI
  my @name_array = split( ":", $F[3] );
  my $umi_id = $name_array[$#name_array];

  # skip this entry if the same UMI is already defined for this position
  if ( defined $cur_pos{$pos}->{$umi_id} ) {
    # potentially, store the read count if it is higher than the so far stored count
    if( $F[4] > $cur_pos{$pos}->{$umi_id}->[4] ) {
      $cur_pos{$pos}->{$umi_id}->[4] = $F[4];
    }
    $collapse_events += 1;
    next;
  }

  # add umi_id to current position
  my @tmp = @F;
  $cur_pos{$pos}->{$umi_id} = \@tmp;
}
close(F);

# deal with last entries
if ( scalar keys %cur_pos == 1 ) {
  foreach my $entry ( keys %cur_pos ) {
    foreach my $umi ( keys %{ $cur_pos{$entry} } ) {

      if( $cur_pos{$entry}->{$umi}->[4] == 1.0 ) {
	$cnt{ "unique" } += 1;
      } elsif ( $cur_pos{$entry}->{$umi}->[4] < 1.0 ) {
	$multimappers{ $cur_pos{$entry}->{$umi}->[3] } = 1;
      } else{
	print STDERR "[ERROR] Current read is neither unique nor multi mapper based on count value\n";
	print STDERR "[ERROR] ", join("\t", @{ $cur_pos{$entry}->{$umi} } ), "\n";
	exit(2);
      }
      print join( "\t", @{ $cur_pos{$entry}->{$umi} } ), "\n";
    }
    %cur_pos = ();
  }
} elsif ( scalar keys %cur_pos > 1 ) {
  print STDERR "[ERROR] More than one previous position was not handled before file ended\n";
  exit(2);
}

# print INFO how many UMI duplicates were merged
print STDERR "[INFO] Number of collapse events due to UMI identity: $collapse_events\n";

# output the stats for unique and multi mappers
open(OUT, "> $count_out") or die "Can't write to $count_out\n";
print OUT "unique_mappers:\t", $cnt{ "unique" },"\n";
print OUT "multi_mappers:\t", scalar keys %multimappers, "\n";
close(OUT);
