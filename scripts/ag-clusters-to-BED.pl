use strict;
use warnings;

# %ordered stores an ordered set of chromosome names
# starting with chr1 ... chrM
my %ordered     = ();
my $idx_ordered = 0;
foreach my $i ( 1 .. 100, 'X', 'Y', 'M' ) {
  foreach my $strand ( '+', '-' ) {
    $idx_ordered++;
    $ordered{"$i:$strand"} = $idx_ordered;
    $idx_ordered++;
    $ordered{"chr$i\_random:$strand"} = $idx_ordered;
  }
}

### ATTENTION ###
# This script expects the input file to be formatted
# according to ag-merge-clusters.pl from the A-seq-processing pipeline
# -> all data is accessed with hard coded indices


my %data = ();
open( F, "gunzip -c $ARGV[0] |" );
while (<F>) {
  next if( $_ =~ /^#/);
  chomp;
  my @F     = split(/\t/);
  my $start = $F[2] - 1;
  my $tpm   = sprintf( "%.3f", $F[5] );
  my $key   = "$F[0]:$F[1]";
  if ( not defined $ordered{$key} ) {
    $data{$key} = [];
    $idx_ordered++;
    $ordered{$key} = $idx_ordered;
  }

  push @{ $data{$key} }, [ $F[0], $start, $F[3], "$F[0]:$F[1]:$F[4]", $tpm, $F[1] ];
}
close(F);

foreach my $key ( sort { $ordered{$a} <=> $ordered{$b} } keys %ordered ) {
  next if ( not defined $data{$key} );
  foreach my $entry ( sort { $a->[1] <=> $b->[1] } @{ $data{$key} } ) {
    print join( "\t", @{$entry} ), "\n";
  }
}
