configfile: "Aseq_config.yaml"

from snakemake.utils import makedirs
from snakemake.utils import listfiles

import numpy as np
import os

# localrules: fastq2fasta,filter_5p_read_start,collapse_full_UMI_duplicates,trim_3p_adapter
localrules: change_fastq_to_fq,create_log_dir,raw_read_cnt,proper_5p_end_read_cnt,no_PCR_dupl_read_cnt,no_3pAdapter_read_cnt,mapped_read_cnt,get_valid_reads,valid_read_cnt,raw_3p_ends_cnt,noIP_3p_ends,noIP_3p_ends_cnt,pooled_3p_sites_cnt,delete_zero_rows,clusters_cnt,finish

################################################################################
# define the target rule
################################################################################

rule finish:
    ## LOCAL ##
    '''
    Defines the target rule by specifying all final output files.
    Additionally, the cluster_logs dir is deleted if
    snakemake was run locally.
    '''
    input: config["results_dir"] + "/counts/annotation_overlap/clusters.summary.tsv"
    params:
        logdir = config["log_dir"]
    run:
        # check if cluster_logs dir for first sample is empty
        # if yes, delete the cluster_logs dir
        file_list = os.listdir("{log}/cluster_logs/".format(log=params.logdir) )
        if len(file_list) == 0:
            shell("rm -r {params.logdir}/cluster_logs")

################################################################################
# chronological processing of the reads
################################################################################

#-------------------------------------------------------------------------------
# create dir for logfiles
#-------------------------------------------------------------------------------

rule create_log_dir:
    ## LOCAL ##
    ''' This step creates the log directory, if necessary.
    This is required when jobs are submitted and the
    job output should be written to these files.
    '''
    output:
        temp( config["results_dir"] + "/created_log.tmp" )
    run:
        makedirs( [config["log_dir"], os.path.join( config["log_dir"], "cluster_logs") ] )
        shell("touch {output}")

#-------------------------------------------------------------------------------
# potential rule: convert file names from fastq.gz to fq.gz
#-------------------------------------------------------------------------------

rule change_fastq_to_fq:
    ## LOCAL ##
    input:
        config["input_dir"] + "/{sample}.fastq.gz"
    output:
        temp(config["input_dir"] + "/{sample}.fq.gz")
    shell:
        '''
        "ln -fs {input} {output}"
        '''

#-------------------------------------------------------------------------------
# convert fastq to fasta
#-------------------------------------------------------------------------------

rule fastq2fasta:
    input:
        config["input_dir"] + "/{sample}.fq.gz",
        config["results_dir"] + "/created_log.tmp"
    output: temp(config["results_dir"] + "/{sample}.fa.gz")
    params:
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    log:
        config["log_dir"] + "/fastq2fasta.{sample}.log"
    shell:
        '''
        perl scripts/ag-convert-FASTQ-to-fasta.pl \
        --fastq_to_fasta=fastq_to_fasta \
        --fastx_renamer=fastx_renamer \
        --outfile={output} \
        {input[0]} \
        2> {log}
        '''

#-------------------------------------------------------------------------------
# get number of raw reads
#-------------------------------------------------------------------------------

rule raw_read_cnt:
    ## LOCAL ##
    input:
        config["results_dir"] + "/{sample}.fa.gz"
    output:
        temp( config["results_dir"] + "/counts/{sample}.raw.nr.out" )
    run:
        import gzip
        n = 0
        with gzip.open(input[0], "rt") as infile:
            n = sum([1 for line in infile if line.startswith(">")])
        with open(output[0], "w") as out:
            out.write("raw_reads:\t%i\n" % n)

#-------------------------------------------------------------------------------
# only retain sites that have the expected 5' end
#-------------------------------------------------------------------------------

rule select_for_valid_5p_configuration:
    input:
        config["results_dir"] + "/{sample}.fa.gz"
    output: temp(config["results_dir"] + "/{sample}.5ptrimmed.UMI_available.fa.gz")
    params:
        adapt=config['read_5p_start'],
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    log:
        config["log_dir"] + "/filter_5p_read_start.{sample}.log"
    shell:
        '''
        (zcat {input} \
        | perl scripts/rs-filter-by-5p-adapter.keep5pAdapter.pl \
        --adapter={params.adapt} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# get number of reads with proper 5' end
#-------------------------------------------------------------------------------

rule proper_5p_end_read_cnt:
    ## LOCAL ##
    '''
    count the reads with proper 5' end; write also the raw read count
    to the output file.
    '''
    input:
        config["results_dir"] + "/counts/{sample}.raw.nr.out",
        config["results_dir"] + "/{sample}.5ptrimmed.UMI_available.fa.gz"
    output:
        temp(config["results_dir"] + "/counts/{sample}.5ptrimmed.nr.out")
    run:
        import gzip
        n = 0
        with gzip.open(input[1], "rt") as infile:
            n = sum([1 for line in infile if line.startswith(">")])
        with open(output[0], "w") as out:
            with open(input[0], "r") as cnt:
                out.write("%s" % cnt.readline() )
            out.write("proper_5p_start:\t%i\n" % n)

#-------------------------------------------------------------------------------
# collapse full UMI-duplicates
#-------------------------------------------------------------------------------

rule collapse_full_UMI_duplicates:
    input: config["results_dir"] + "/{sample}.5ptrimmed.UMI_available.fa.gz"
    output: temp( config["results_dir"] + "/{sample}.UMI_dupl_removed.fa.gz" )
    params:
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    resources:
        mem = 10
    log:
        config["log_dir"] + "/collapse_full_UMI_duplicates.{sample}.log"
    shell:
        '''
        (zcat {input} \
        | perl scripts/rs-collapse-UMIduplicates.keepUMI.pl \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# get number of reads after full PCR duplicate filtering
#-------------------------------------------------------------------------------

rule no_PCR_dupl_read_cnt:
    ## LOCAL ##
    '''
    count the reads without full PCR duplicaes; write also the raw read count
    to the output file.
    '''
    input:
        config["results_dir"] + "/counts/{sample}.5ptrimmed.nr.out",
        config["results_dir"] + "/{sample}.UMI_dupl_removed.fa.gz"
    output:
        temp(config["results_dir"] + "/counts/{sample}.no_full_PCR_dupl.nr.out")
    run:
        import gzip
        n = 0
        with gzip.open(input[1], "rt") as infile:
            n = sum([1 for line in infile if line.startswith(">")])
        with open(output[0], "w") as out:
            with open(input[0], "r") as cnt:
                for line in cnt:
                    out.write("%s" % line )
            out.write("no_PCR_duplicates:\t%i\n" % n)

#-------------------------------------------------------------------------------
# trim 3' adapter, only keep reads with a minimum length
#-------------------------------------------------------------------------------

rule trim_3p_adapter:
    input: config["results_dir"] + "/{sample}.UMI_dupl_removed.fa.gz"
    output: temp( config["results_dir"] + "/{sample}.trimmed.UMI.fa.gz" )
    params:
        adapt=config['3p_adapter'],
        minLen=config['min_length'],
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    log:
        config["log_dir"] + "/trim_3p_adapter.{sample}.log"
    shell:
        '''
        (cutadapt -a {params.adapt} \
        --minimum-length {params.minLen} \
        {input} \
        | fastx_reverse_complement \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# get number of reads after 3' adapter trimming
#-------------------------------------------------------------------------------

rule no_3pAdapter_read_cnt:
    ## LOCAL ##
    '''
    count the reads after the 3' adapter was trimmed; 
    write also the raw read count to the output file.
    '''
    input:
        config["results_dir"] + "/counts/{sample}.no_full_PCR_dupl.nr.out",
        config["results_dir"] + "/{sample}.trimmed.UMI.fa.gz"
    output:
        temp(config["results_dir"] + "/counts/{sample}.trimmed.nr.out")
    run:
        import gzip
        n = 0
        with gzip.open(input[1], "rt") as infile:
            n = sum([1 for line in infile if line.startswith(">")])
        with open(output[0], "w") as out:
            with open(input[0], "r") as cnt:
                for line in cnt:
                    out.write("%s" % line )
            out.write("trimmed:\t%i\n" % n)

#-------------------------------------------------------------------------------
# collect valid reads
#-------------------------------------------------------------------------------

rule get_valid_reads:
    ## LOCAL ##
    '''
    valid reads have:
    not more than 2 Ns
    maximum 80% A-content
    a 3' nucleotide other than A
    '''
    input:
        config["results_dir"] + "/{sample}.trimmed.UMI.fa.gz"
    output:
        temp( config["results_dir"] + "/{sample}.valid.trimmed.UMI.fa.gz" )
    params:
        maxN = config['maxN'],
        maxAcontent = config['maxAcontent']
    log:
       config["log_dir"] + "/get_valid_reads.{sample}.log"
    shell:
        '''
        (zcat {input[0]} \
        | perl scripts/ag-filter-seqs-by-nucleotide-composition.pl --max {params.maxN} --nuc N \
        | perl scripts/ag-filter-seqs-by-nucleotide-composition.pl --max {params.maxAcontent} --nuc A  \
        | perl scripts/ag-filter-seqs-by-last-nuc.pl \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# count valid reads
#-------------------------------------------------------------------------------

rule valid_read_cnt:
    ## LOCAL ##
    '''
    count the reads after filtering for valid read configuration; 
    write also the raw read count to the output file.
    '''
    input:
        config["results_dir"] + "/counts/{sample}.trimmed.nr.out",
        config["results_dir"] + "/{sample}.valid.trimmed.UMI.fa.gz"
    output:
        temp(config["results_dir"] + "/counts/{sample}.valid.nr.out")
    run:
        import gzip
        n = 0
        with gzip.open(input[1], "rt") as infile:
            n = sum([1 for line in infile if line.startswith(">")])
        with open(output[0], "w") as out:
            with open(input[0], "r") as cnt:
                for line in cnt:
                    out.write("%s" % line )
            out.write("valid_composition:\t%i\n" % n)

#-------------------------------------------------------------------------------
# create genome index for STAR mapping
#-------------------------------------------------------------------------------

rule create_STAR_index:
    output:
        config['STAR_idx'] + "/SAindex"
    params:
        read_len = lambda wildcards: int(config['read_length']) - 7 - 1,
        genome = config['genome'],
        gtf = config['gene_annotation'],
        idx_dir = config['STAR_idx'],
        cluster_log = config["log_dir"] + "/cluster_logs/creat_STAR_index.log"
    threads: config["threads.STAR"]
    resources:
        mem = lambda wildcards: int(np.ceil( config['STAR.max.RAM'] / config['threads.STAR'] ) )
    log:
        config["log_dir"] + "/create_STAR_index.log"
    shell:
        '''
        if [[ ! -d {params.idx_dir} ]]; then
        mkdir {params.idx_dir}
        fi
        STAR \
        --runMode genomeGenerate \
        --sjdbOverhang {params.read_len} \
        --genomeDir {params.idx_dir} \
        --genomeFastaFiles {params.genome} \
        --runThreadN {threads} \
        --sjdbGTFfile {params.gtf} \
        &> {log}
        '''

#-------------------------------------------------------------------------------
# map the reads
#-------------------------------------------------------------------------------

rule mapping:
    input:
        config["results_dir"] + "/{sample}.valid.trimmed.UMI.fa.gz",
        config['STAR_idx'] + "/SAindex"
    output:
        config["results_dir"] + "/{sample}.STAR_out/Aligned.sortedByCoord.out.bam"
    params:
        anno = config['gene_annotation'],
        outdir = config["results_dir"] + "/{sample}.STAR_out/",
        idx_dir = config['STAR_idx'],
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    resources:
        mem = lambda wildcards: int(np.ceil( config['STAR.max.RAM'] / config['threads.STAR'] ) )
    threads: config['threads.STAR']
    log:
        config["log_dir"] + "/mapping.{sample}.log"
    shell:
        '''
        if [[ ! -d {params.outdir} ]]; then
        mkdir {params.outdir}
        fi
        STAR \
	--runMode alignReads \
	--twopassMode Basic \
	--runThreadN {threads} \
	--genomeDir {params.idx_dir} \
	--sjdbGTFfile {params.anno} \
	--readFilesIn {input[0]} \
	--outFileNamePrefix {params.outdir} \
	--outSAMtype BAM SortedByCoordinate \
	--limitBAMsortRAM 31532137230 \
	--readFilesCommand zcat \
	--outSAMstrandField intronMotif \
	--alignIntronMax 200000 \
	--outReadsUnmapped Fastx \
	--outSAMattributes All \
	--alignEndsType EndToEnd \
        &> {log}
        '''

#-------------------------------------------------------------------------------
# convert the BAM file into BED format
#-------------------------------------------------------------------------------

rule bam2bed:
    input:
        config["results_dir"] + "/{sample}.STAR_out/Aligned.sortedByCoord.out.bam"
    output:
        temp( config["results_dir"] + "/{sample}.reads.bed.gz" )
    params:
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    resources:
        mem=5
    threads: config['threads.bam2bed']
    log:
        config["log_dir"] + "/bam2bed.{sample}.log"
    shell:
        '''
        (samtools view {input} \
        | python scripts/rs-bam2bed.py \
        --processors {threads} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# from multiple mappings of one read-id, only the keep these with the lowest
# edit distance (this might be again more than one)
#-------------------------------------------------------------------------------

rule select_by_edit_distance:
    input:
        config["results_dir"] + "/{sample}.reads.bed.gz"
    output:
        temp( config["results_dir"] + "/{sample}.reads.edit_distance_filtered.bed.gz" )
    params:
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    resources:
        mem = 10
    log:
       config["log_dir"] + "/select_by_edit_distance.{sample}.log"
    shell:
        '''
        (python scripts/rs-select-min-edit-distance.py \
        --bed {input} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# remove UMI/PCR duplicates; count unique/multimappers
#-------------------------------------------------------------------------------

rule collapse_mapped_UMI_duplicates:
    input:
        config["results_dir"] + "/{sample}.reads.edit_distance_filtered.bed.gz"
    output:
        temp(config["results_dir"] + "/{sample}.reads.collapsed.bed.gz"),
        temp(config["results_dir"] + "/counts/{sample}.mapped.nr.out")
    params:
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    log:
       config["log_dir"] + "/collapse_mapped_UMI_duplicates.{sample}.log"
    shell:
        '''
        (perl scripts/rs-collapse-mapped-UMIduplicates.pl \
        --count_out={output[1]} \
        {input} \
        | gzip > {output[0]}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# count mapped reads
#-------------------------------------------------------------------------------

rule mapped_read_cnt:
    ## LOCAL ###
    '''The mapped reads were counted already. This
    rule simply merges the count with previous counts.
    '''
    input:
        config["results_dir"] + "/counts/{sample}.valid.nr.out",
        config["results_dir"] + "/counts/{sample}.mapped.nr.out"
    output:
        temp(config["results_dir"] + "/counts/{sample}.mapped.noUMI_duplicates.nr.out")
    run:        
        with open(output[0], "w") as out:
            for f in input:
                with open(f, "r") as cnt:
                    for line in cnt:
                        out.write("%s" % line )

#-------------------------------------------------------------------------------
# get 3' ends
#-------------------------------------------------------------------------------

rule get_3p_ends:
    '''Only 3' ends with the following characteristics are reported:
    minimum the last 4 nt map perfectly to the genome
    the read was found to be valid before
    '''
    input:
        config["results_dir"] + "/{sample}.reads.collapsed.bed.gz"
    output:
        temp( config["results_dir"] + "/{sample}.3pSites.bed.gz")
    params:
        exclude_chr=lambda wildcards: "--exclude=" + ":".join(config['excluded_chr']) if config['excluded_chr'] is not None else "",
        min_align = config['min_3p_align'],
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    resources:
        mem = 10
    log:
        config["log_dir"] + "/get_3p_ends.{sample}.log"
    shell:
        '''
        (perl scripts/rs-get-3pEnds-from-bed.pl \
        {params.exclude_chr} \
        --strict \
        --min_align={params.min_align} \
        {input} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# count the number of single 3' ends
#-------------------------------------------------------------------------------

rule raw_3p_ends_cnt:
    ## LOCAL ##
    input:
        config["results_dir"] + "/counts/{sample}.mapped.noUMI_duplicates.nr.out",
        config["results_dir"] + "/{sample}.3pSites.bed.gz"
    output:
        temp( config["results_dir"] + "/counts/{sample}.raw_3p_ends.nr.out" )
    run:
        import gzip
        n = 0
        with gzip.open(input[1], "rt") as infile:
            n = sum([1 for line in infile])
        with open(output[0], "w") as out:
            with open(input[0], "r") as cnt:
                for line in cnt:
                    out.write("%s" % line )
            out.write("3p_ends_raw:\t%i\n" % n)

#-------------------------------------------------------------------------------
# extract the sequences that surround the 3' ends
#-------------------------------------------------------------------------------

rule fetch_flanking_seqs:
    input:
        config["results_dir"] + "/{sample}.3pSites.bed.gz",
        config["results_dir"] + "/counts/{sample}.raw_3p_ends.nr.out"
    output:
        temp( config["results_dir"] + "/{sample}.3pSites.bed.seqs.gz" )
    params:
        upstream_ext = config['upstream_region.IP'],
        downstream_ext = config['downstream_region.IP'],
        genome = config["genome"],
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    resources:
        mem = 10
    log:
        config["log_dir"] + "/fetch_flanking_seqs.{sample}.log"
    shell:
        '''
        (perl scripts/rs-fetch-flanking-fasta.pl \
        --genome={params.genome} \
        --upstream={params.upstream_ext} \
        --downstream={params.downstream_ext} \
        {input[0]} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# assign internal priming sites
#-------------------------------------------------------------------------------

rule assign_IP_sites:
    input:
        config["results_dir"] + "/{sample}.3pSites.bed.seqs.gz"
    output:
        config["results_dir"] + "/{sample}.3pSites.ip.bed.gz"
    params:
        upstream_ext = config['upstream_region.IP'],
        downstream_ext = config['downstream_region.IP'],
        tot_As = config['total_As'],
        consec_As = config['consecutive_As'],
        ds_patterns = lambda wildcards: " ".join(["--ds_pattern=%s" % pat for pat in config['downstream_patterns'] ]) if config['downstream_patterns'] is not None else "",
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    log:
        config["log_dir"] + "/assign_IP_sites.{sample}.log"
    shell:
        '''
        (perl scripts/ag-assign-internal-priming-sites.pl \
        --upstream_len={params.upstream_ext} \
        --downstream_len={params.downstream_ext} \
        --consecutive_As={params.consec_As} \
        --total_As={params.tot_As} \
        {params.ds_patterns} \
        {input} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# get a file that does not contain the IP-marked sites
#-------------------------------------------------------------------------------

rule noIP_3p_ends:
    ### LOCAL ###
    input:
        config["results_dir"] + "/{sample}.3pSites.ip.bed.gz"
    output:
        config["results_dir"] + "/{sample}.3pSites.noIP.bed.gz"
    log:
        config["log_dir"] + "/noIP_3p_ends.{sample}.log"
    shell:
        '''
        (zcat {input} \
        | perl -ne 'my @F = split("\t"); print $_ if($F[3]) ne IP;' \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# count the number of valid 3' ends (no internal priming)
#-------------------------------------------------------------------------------

rule noIP_3p_ends_cnt:
    ## LOCAL ##
    input:
        config["results_dir"] + "/counts/{sample}.raw_3p_ends.nr.out",
        config["results_dir"] + "/{sample}.3pSites.noIP.bed.gz"
    output:
        config["results_dir"] + "/counts/{sample}.summary.tsv"
    run:
        import gzip
        n = 0
        with gzip.open(input[1], "rt") as infile:
            n = sum([1 for line in infile])
        with open(output[0], "w") as out:
            with open(input[0], "r") as cnt:
                for line in cnt:
                    out.write("%s" % line )
            out.write("3p_ends_raw_noIP:\t%i\n" % n)

#-------------------------------------------------------------------------------
# get statistics for the gtf-features these sites overlap with
#-------------------------------------------------------------------------------

rule sites_feature_overlap_stats:
    '''annotated are: intron, exon, terminal exon, 3' UTR, intergenic
    note: if 3' UTR and 5' UTR are labeled as one in the gtf, UTR would
    include also sites falling into 5' UTR regions but this portion is
    expected to be very small
    '''

    input:
        config["results_dir"] + "/counts/{sample}.summary.tsv",
        config["results_dir"] + "/{sample}.3pSites.noIP.bed.gz"
    output:
        config["results_dir"] + "/counts/annotation_overlap/{sample}.raw_3p_ends.noIP.summary.tsv"
    params:
        gtf = config['gene_annotation'],
        utr_name = config['utr_name'],
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    log:
        config["log_dir"] + "/sites_feature_overlap_stats.{sample}.log"
    shell:
        '''
        python scripts/rs-sites-to-feature-annotation.py \
        --gtf {params.gtf} \
        --utr_name {params.utr_name} \
        --bed {input[1]} \
        --verbose \
        > {output} 2> {log}
        '''

#-------------------------------------------------------------------------------
# merge all samples to a full set of 3' end sites
#-------------------------------------------------------------------------------

rule pool_samples:
    input:
        files = expand( config["results_dir"] + "/{sample}.3pSites.ip.bed.gz", sample=config['samples']),
        counts = expand( config["results_dir"] + "/counts/annotation_overlap/{sample}.raw_3p_ends.noIP.summary.tsv", sample=config['samples'])
    output:
        config["results_dir"] + "/3pSites.tsv.gz"
    params:
        cluster_log = config["log_dir"] + "/cluster_logs/clustering_process.log"
    resources:
        mem = lambda wildcards: int( 1.5 * len(config['samples']) )
    log:
        config["log_dir"] + "/pool_samples.log"
    shell:
        '''
        (perl scripts/ag-pool-sites.pl \
        --noip \
        {input.files} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# get overall number of 3' end sites (across all samples)
#-------------------------------------------------------------------------------

rule pooled_3p_sites_cnt:
    ## LOCAL ##
    input:
        config["results_dir"] + "/3pSites.tsv.gz"
    output:
        temp( config["results_dir"] + "/counts/pooled_3p_ends.nr.out" )
    run:
        import gzip
        n = 0
        with gzip.open(input[0], "rt") as infile:
            n = sum([1 for line in infile if not line.startswith("#")])
        with open(output[0], "w") as out:
            out.write("pooled_3p_ends:\t%i\n" % n)

# ## NOT USED CURRENTLY
# #-------------------------------------------------------------------------------
# # get all sites that have been discarded as internal prmining artefacts
# #-------------------------------------------------------------------------------

# rule collect_ip_sites:
#     input:
#         expand( config["results_dir"] + "/{sample}.3pSites.ip.bed.gz", sample=config['samples'])
#     output:
#         config["results_dir"] + "/3p_IP_Sites.bed.gz"
#     params:
#         logdir = config["log_dir"]
#     log:
#         config["log_dir"] + "/collect_ip_sites.log"
#     shell:
#         '''
#         perl scripts/ag-get-known-IPsites.pl \
#         {input} \
#         | gzip > {output}
#         '''

#-------------------------------------------------------------------------------
# assign poly(A) signals
#-------------------------------------------------------------------------------

rule assign_polyA_signals:
    '''
    Assign poly(A) signals to the 3' end sites. Check for signals in the region
    of -60 to +10 around each 3' end site.
    '''
    input:
        # ip_sites = config["results_dir"] + "/3p_IP_Sites.bed.gz",
        three_p_sites = config["results_dir"] + "/3pSites.tsv.gz"
    output:
        config["results_dir"] + "/3pSites.PAS.tsv.gz"
    params:
        signals = lambda wildcards: " ".join(["--motif=%s" % sig for sig in config['polyA.signals'] ]),
        genome = config['genome'],
        cluster_log = config["log_dir"] + "/cluster_logs/clustering_process.log"
    log:
        config["log_dir"] + "/assign_polyA_signals.log"
    shell:
        '''
        (perl scripts/ag-assign-polyAsignals.pl \
        {params.signals} \
        --genome={params.genome} \
        {input.three_p_sites} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# define sample-specific background
#-------------------------------------------------------------------------------

rule sample_specific_bg:
    '''Based on the annotated poly(A) signals, determine the minimum number
    of reads per 3' end such that among all 3' end sites over the threshold
    x % have at least one annotated poly(A) signal
    '''
    input:
        config["results_dir"] + "/3pSites.PAS.tsv.gz"
    output:
         temp( config["results_dir"] + "/filteredSites/{sample}.filtered.tsv" )
    params:
        cutoff = config['polyAsignal_cutoff'],
        upstream_reg = config['upstream_bg_clusters'],
        downstream_reg = config['downstream_bg_clusters'],
        cluster_log = lambda wildcards: config["log_dir"] + "/cluster_logs/" + wildcards.sample + ".log"
    resources:
        mem = 10
    log:
        config["log_dir"] + "/sample_specific_bg.{sample}.log"
    shell:
        '''
        perl scripts/rs-find-sample-specific-cutoff.pl \
        --cutoff={params.cutoff} \
        --upstream={params.upstream_reg} \
        --downstream={params.downstream_reg} \
        --sample={wildcards.sample} \
        {input} \
        > {output} 2> {log}
        '''

#-------------------------------------------------------------------------------
# merge the sample-specific results to a new overall table of 3' end sites
#-------------------------------------------------------------------------------

rule create_noBG_3pSites_table:
    input:
        filtered = expand( config["results_dir"] + "/filteredSites/{sample}.filtered.tsv", sample=config['samples']),
        raw_table = config["results_dir"] + "/3pSites.PAS.tsv.gz"
    output:
        table_adjusted = temp(config["results_dir"] + "/3pSites.PAS.filtered.tsv.gz")
    params:
        cluster_log = config["log_dir"] + "/cluster_logs/create_noBG_3pSites_table.log"
    resources:
        mem = 10
    log:
        config["log_dir"] + "/create_noBG_3pSites_table.log"
    script:
        "scripts/merge-sample-bg-files.py"

#-------------------------------------------------------------------------------
# delete 3' end sites without cutoff-corrected read support from any sample
#-------------------------------------------------------------------------------

rule delete_zero_rows:
    ## LOCAL ##
    input:
        config["results_dir"] + "/3pSites.PAS.filtered.tsv.gz"
    output:
        temp( config["results_dir"] + "/3pSites.PAS.noBG.tsv.gz" )
    log:
        config["log_dir"] + "/delete_zero_rows.log"
    run:
        import gzip

        with gzip.open(output[0], "wt") as out_file:
            with gzip.open(input[0], "rt") as infile:
                for line in infile:
                    if line.startswith("#"):
                        out_file.write(line)
                        continue
                    line_list = line.rstrip().split("\t")
                    read_sum = sum( [1 for i in line_list[3:-2] if float(i) > 0] )
                    if read_sum > 0:
                        # this site has still read support
                        out_file.write(line)

#-------------------------------------------------------------------------------
# cluster individual closely spaced 3' end sites
#-------------------------------------------------------------------------------

rule cluster_sites:
    input:
        config["results_dir"] + "/3pSites.PAS.noBG.tsv.gz"
    output:
        temp( config["results_dir"] + "/clusters.primary.tsv.gz" )
    params:
        upstream_ext = config['upstream_clusters'],
        downstream_ext = config['downstream_clusters'],
        cluster_log = config["log_dir"] + "/cluster_logs/clustering_process.log"
    log:
        config["log_dir"] + "/cluster_sites.log"
    shell:
        '''
        (perl scripts/ag-generate-clusters.pl \
        --upstream={params.upstream_ext} \
        --downstream={params.downstream_ext} \
        {input} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# merge closely spaced clusters
#-------------------------------------------------------------------------------

rule merge_clusters:
    '''cluster are further merged if: 
    - an IP candidate has another downstream cluster that shares all its
    PAS with the IP candidate
    - a cluster shares all its PAS with the next cluster upstream
    - two clusters without independent PAS have a combined length smaller the maxsize
    Keep all un-merged clusters without PAS
    '''
    input:
        config["results_dir"] + "/clusters.primary.tsv.gz"
    output:
        config["results_dir"] + "/clusters.merged.tsv.gz"
    params:
        maxsize = config['max_cluster_size'],
        minDistToPAS = config['min_dist_to_PAS'],
        cluster_log = config["log_dir"] + "/cluster_logs/clustering_process.log"
    log:
        config["log_dir"] + "/merge_clusters.log"
    shell:
        '''
        (perl scripts/ag-merge-clusters.pl \
        --minDistToPAS={params.minDistToPAS} \
        --maxsize={params.maxsize} \
        {input} \
        | gzip > {output}) 2> {log}
        '''

#-------------------------------------------------------------------------------
# convert clusters to proper BED format
#-------------------------------------------------------------------------------

rule clusters_to_bed:
    input:
        config["results_dir"] + "/clusters.merged.tsv.gz"
    output:
        config["results_dir"] + "/clusters.merged.bed"
    params:
        cluster_log = config["log_dir"] + "/cluster_logs/clustering_process.log"
    log:
        config["log_dir"] + "/clusters_to_bed.log"
    shell:
        '''
        perl scripts/ag-clusters-to-BED.pl \
        {input} \
        > {output} 2> {log}
        '''

#-------------------------------------------------------------------------------
# count number of clusters
#-------------------------------------------------------------------------------

rule clusters_cnt:
    ## LOCAL ##
    input:
        config["results_dir"] + "/counts/pooled_3p_ends.nr.out",
        config["results_dir"] + "/clusters.merged.bed"
    output:
        config["results_dir"] + "/counts/clusters.nr.out"
    run:
        n = 0
        with open(input[1], "r") as infile:
            n = sum([1 for line in infile])
        with open(output[0], "w") as out:
            with open(input[0], "r") as cnt:
                for line in cnt:
                    out.write("%s" % line )
            out.write("clusters:\t%i\n" % n)

#-------------------------------------------------------------------------------
# get statistics for the gtf-features the representative sites overlap with
#-------------------------------------------------------------------------------

rule clusters_feature_overlap_stats:
    '''annotated are: intron, exon, terminal exon, 3' UTR, intergenic
    note: if 3' UTR and 5' UTR are labeled as one in the gtf, UTR would
    include also sites falling into 5' UTR regions but this portion is
    expected to be very small. Note: only the representative site is
    used for the intersection with features.
    '''

    input:
        config["results_dir"] + "/counts/clusters.nr.out",
        config["results_dir"] + "/clusters.merged.bed"
    output:
        config["results_dir"] + "/counts/annotation_overlap/clusters.summary.tsv"
    params:
        gtf = config['gene_annotation'],
        utr_name = config['utr_name'],
        cluster_log = config["log_dir"] + "/cluster_logs/clustering_process.log"
    log:
        config["log_dir"] + "/clusters_feature_overlap_stats.log"
    shell:
        '''
        python scripts/rs-clusters-to-feature-annotation.py \
        --gtf {params.gtf} \
        --utr_name {params.utr_name} \
        --bed {input[1]} \
        --verbose \
        > {output} 2> {log}
        '''
